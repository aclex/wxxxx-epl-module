/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <epl/element/wiznet/w5500/register_map.h>
#include <epl/element/wiznet/w5500/interrupt.h>
#include <epl/element/wiznet/w5500/io.h>

template<class Port, std::size_t N> epl::element::wiznet::w5500::protocol epl::element::wiznet::w5500::socket<Port, N>::protocol() noexcept
{
	const auto loaded{read<Port>(s_mode_address, register_map::socket<N>::control, 1)};
	return static_cast<epl::element::wiznet::w5500::protocol>(loaded[0] & 0x0f);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_protocol(const epl::element::wiznet::w5500::protocol p) noexcept
{
	auto loaded{read<Port>(s_mode_address, register_map::socket<N>::control, 1)};
	loaded[0] = loaded[0] & 0xf0 | static_cast<std::uint8_t>(p);
	write<Port>(s_mode_address, register_map::socket<N>::control, std::span<std::uint8_t, 1>(loaded));
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::open() noexcept
{
	send(command::open);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::close() noexcept
{
	send(command::close);
	auto loaded{read<Port>(s_mode_address, register_map::socket<N>::control, 1)};
	loaded[0] &= 0xf0;
	write<Port>(s_mode_address, register_map::socket<N>::control, std::span<std::uint8_t, 1>(loaded));
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::send(const command cmd) noexcept
{
	std::array<std::uint8_t, 1> data{static_cast<std::uint8_t>(cmd)};
	write<Port>(s_command_address, register_map::socket<N>::control, std::span{data});

	const auto expected{success_result(cmd)};

	if (expected != interrupt::socket::flag::no)
	{
		wait_for_interrupt(expected);
	}
}

template<class Port, std::size_t N> template<epl::element::wiznet::byte_wide T, std::size_t S> std::size_t epl::element::wiznet::w5500::socket<Port, N>::send(const std::span<T, S> data) noexcept
{
	const auto reported_free_size{tx_buffer_free_size()};

	std::size_t safe_write_size{reported_free_size};

	const auto packet_size{std::min(safe_write_size, data.size())};
	const auto ptr{tx_buffer_write_ptr()};
	const auto packet{data.subspan(0, packet_size)};
	write<Port>(ptr, register_map::socket<N>::tx, packet);
	set_tx_buffer_write_ptr(static_cast<std::uint16_t>(static_cast<std::uint32_t>(ptr + packet_size) & 0xffff));
	send(command::send);

	return packet_size;
}

template<class Port, std::size_t N> std::vector<std::uint8_t> epl::element::wiznet::w5500::socket<Port, N>::recv(const std::size_t size, const bool no_less) noexcept
{
	std::vector<std::uint8_t> dest(size);

	const auto received{recv(std::span{dest}, no_less)};

	if (received < size)
		dest.resize(received);

	return dest;
}

template<class Port, std::size_t N> template<epl::element::wiznet::byte_wide T, std::size_t S> std::size_t epl::element::wiznet::w5500::socket<Port, N>::recv(const std::span<T, S> dest, const bool no_less) noexcept
{
	const auto reported_received_size{wait_for_received_size(dest.size(), no_less)};

	const auto packet_size{std::min(reported_received_size, dest.size())};
	const auto ptr{rx_buffer_read_ptr()};
	auto result{read<Port>(ptr, register_map::socket<N>::rx, dest)};
	set_rx_buffer_read_ptr(static_cast<std::uint16_t>(static_cast<std::uint32_t>(ptr + packet_size) & 0xffff));

	send(command::recv);

	return result;
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::socket<Port, N>::available_to_read() noexcept
{
	return rx_buffer_received_size();
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::socket<Port, N>::available_to_write() noexcept
{
	return tx_buffer_free_size();
}

template<class Port, std::size_t N> epl::element::wiznet::w5500::status epl::element::wiznet::w5500::socket<Port, N>::status() noexcept
{
	const auto loaded{read<Port>(s_status_address, register_map::socket<N>::control, 1)};
	return epl::element::wiznet::w5500::status{loaded[0]};
}

template<class Port, std::size_t N> unsigned int epl::element::wiznet::w5500::socket<Port, N>::source_port() noexcept
{
	const auto loaded{read<Port>(s_source_port_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_source_port(const unsigned int p) noexcept
{
	std::array<std::uint8_t, 2> data{static_cast<std::uint8_t>((p >> 8) & 0xff), static_cast<std::uint8_t>(p & 0xff)};
	write<Port>(s_source_port_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> unsigned int epl::element::wiznet::w5500::socket<Port, N>::destination_port() noexcept
{
	const auto loaded{read<Port>(s_destination_port_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_destination_port(const unsigned int p) noexcept
{
	std::array<std::uint8_t, 2> data{static_cast<std::uint8_t>((p >> 8) & 0xff), static_cast<std::uint8_t>(p & 0xff)};
	write<Port>(s_destination_port_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> epl::net::address::ip::v4 epl::element::wiznet::w5500::socket<Port, N>::destination_address() noexcept
{
	net::address::ip::v4 result;
	const auto loaded{read<Port>(s_destination_address_address, register_map::socket<N>::control, 4)};
	copy(begin(loaded), end(loaded), begin(result));

	return result;
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_destination_address(net::address::ip::v4& addr) noexcept
{
	write<Port>(s_destination_address_address, register_map::socket<N>::control, std::span{addr});
}

template<class Port, std::size_t N> epl::net::address::mac epl::element::wiznet::w5500::socket<Port, N>::destination_hardware_address() noexcept
{
	net::address::mac result;
	const auto loaded{read<Port>(s_destination_hardware_address_address, register_map::socket<N>::control, 6)};
	copy(begin(loaded), end(loaded), begin(result));

	return result;
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_destination_hardware_address(net::address::mac& addr) noexcept
{
	write<Port>(s_destination_hardware_address_address, register_map::socket<N>::control, std::span{addr});
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::socket<Port, N>::mss() noexcept
{
	const auto loaded{read<Port>(s_mss_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_mss(const std::size_t v) noexcept
{
	std::array<std::uint8_t, 2> data{static_cast<std::uint8_t>((v >> 8) & 0xff), static_cast<std::uint8_t>(v & 0xff)};
	write<Port>(s_mss_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> epl::element::wiznet::w5500::buffer_size epl::element::wiznet::w5500::socket<Port, N>::rx_buffer_size() noexcept
{
	const auto loaded{read<Port>(s_rx_buffer_size_address, register_map::socket<N>::control, 1)};
	return static_cast<buffer_size>(loaded[0]);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_rx_buffer_size(const buffer_size v) noexcept
{
	std::array<std::uint8_t, 1> data{static_cast<std::uint8_t>(v)};
	write<Port>(s_rx_buffer_size_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> epl::element::wiznet::w5500::buffer_size epl::element::wiznet::w5500::socket<Port, N>::tx_buffer_size() noexcept
{
	const auto loaded{read<Port>(s_tx_buffer_size_address, register_map::socket<N>::control, 1)};
	return static_cast<buffer_size>(loaded[0]);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_tx_buffer_size(const buffer_size v) noexcept
{
	std::array<std::uint8_t, 1> data{static_cast<std::uint8_t>(v)};
	write<Port>(s_tx_buffer_size_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::enable_interrupt_handling(std::unique_ptr<interrupt::socket::basic_handler<N>>&& h) noexcept
{
	if (h)
	{
		interrupt::socket::basic_handler<N>::set_instance(std::move(h));
		device<Port>::template enable_socket_interrupt<N>();
	}
	else
		emplace_interrupt_handling<interrupt::socket::basic_handler<N>>();
}

template<class Port, std::size_t N> template<class T, class... Args> void epl::element::wiznet::w5500::socket<Port, N>::emplace_interrupt_handling(Args&&... args) noexcept
{
	interrupt::socket::basic_handler<N>::template emplace_instance<T>(std::forward<Args>(args)...);
	device<Port>::template enable_socket_interrupt<N>();
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::disable_interrupt_handling() noexcept
{
	device<Port>::template disable_socket_interrupt<N>();
}

template<class Port, std::size_t N> epl::element::wiznet::w5500::interrupt::socket::flag epl::element::wiznet::w5500::socket<Port, N>::interrupt() noexcept
{
	const auto loaded{read<Port>(s_interrupt_address, register_map::socket<N>::control, 1)};
	return static_cast<interrupt::socket::flag>(loaded[0]);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::clear_interrupt(const interrupt::socket::flag f) noexcept
{
	using namespace interrupt::socket;

	std::array<std::uint8_t, 1> data{static_cast<std::uint8_t>(f)};
	write<Port>(s_interrupt_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::handle_interrupt() noexcept
{
	using namespace interrupt::socket;

	const auto requests{interrupt()};

	auto* const h{basic_handler<N>::instance()};

	if (h)
		h->on(requests);

	clear_interrupt(requests);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::wait_for_interrupt(const interrupt::socket::flag f) noexcept
{
	using namespace interrupt::socket;

	auto* const h{basic_handler<N>::instance()};

	if (h)
	{
		while (!(h->interrupt() & f))
			epl::irq::unit::wait_for_interrupt();

		h->clear(f);
	}
	else
	{
		while (!(interrupt() & f));
	}
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::socket<Port, N>::tx_buffer_free_size() noexcept
{
	using namespace std;

	vector<std::uint8_t> last_loaded;
	while(true)
	{
		const auto loaded{read<Port>(s_tx_buffer_free_size_address, register_map::socket<N>::control, 2)};

		if (last_loaded != loaded)
			last_loaded = loaded;
		else
			break;
	}

	return (static_cast<unsigned int>(last_loaded[0]) << 8) | last_loaded[1];
}

template<class Port, std::size_t N> std::uint16_t epl::element::wiznet::w5500::socket<Port, N>::tx_buffer_read_ptr() noexcept
{
	const auto loaded{read<Port>(s_tx_buffer_read_ptr_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> std::uint16_t epl::element::wiznet::w5500::socket<Port, N>::tx_buffer_write_ptr() noexcept
{
	const auto loaded{read<Port>(s_tx_buffer_write_ptr_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_tx_buffer_write_ptr(const std::uint16_t v) noexcept
{
	std::array<std::uint8_t, 2> data{static_cast<std::uint8_t>((v >> 8) & 0xff), static_cast<std::uint8_t>(v & 0xff)};
	write<Port>(s_tx_buffer_write_ptr_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::socket<Port, N>::rx_buffer_received_size() noexcept
{
	using namespace std;

	vector<std::uint8_t> last_loaded;
	while(true)
	{
		const auto loaded{read<Port>(s_rx_buffer_received_size_address, register_map::socket<N>::control, 2)};

		if (last_loaded != loaded)
			last_loaded = loaded;
		else
			break;
	}

	return (static_cast<unsigned int>(last_loaded[0]) << 8) | last_loaded[1];
}

template<class Port, std::size_t N> std::uint16_t epl::element::wiznet::w5500::socket<Port, N>::rx_buffer_read_ptr() noexcept
{
	const auto loaded{read<Port>(s_rx_buffer_read_ptr_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::socket<Port, N>::set_rx_buffer_read_ptr(const std::uint16_t v) noexcept
{
	std::array<std::uint8_t, 2> data{static_cast<std::uint8_t>((v >> 8) & 0xff), static_cast<std::uint8_t>(v & 0xff)};
	write<Port>(s_rx_buffer_read_ptr_address, register_map::socket<N>::control, std::span{data});
}

template<class Port, std::size_t N> std::uint16_t epl::element::wiznet::w5500::socket<Port, N>::rx_buffer_write_ptr() noexcept
{
	const auto loaded{read<Port>(s_rx_buffer_write_ptr_address, register_map::socket<N>::control, 2)};
	return (static_cast<unsigned int>(loaded[0]) << 8) | loaded[1];
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::socket<Port, N>::wait_for_received_size(const std::size_t s, bool no_less) noexcept
{
	const auto expected{interrupt::socket::flag::recv};

	std::size_t reported_received_size{};
	while ((!no_less && !reported_received_size) || (no_less && reported_received_size < s))
	{
		wait_for_interrupt(expected);
		reported_received_size = rx_buffer_received_size();
	}

	return reported_received_size;
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::tcp::socket<Port, N>::open() noexcept
{
	w5500::socket<Port, N>::set_protocol(protocol::tcp);
	w5500::socket<Port, N>::open();
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::tcp::socket<Port, N>::connect() noexcept
{
	w5500::socket<Port, N>::send(command::connect);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::tcp::socket<Port, N>::disconnect() noexcept
{
	w5500::socket<Port, N>::send(command::discon);
}

template<class Port, std::size_t N> void epl::element::wiznet::w5500::udp::socket<Port, N>::open() noexcept
{
	w5500::socket<Port, N>::set_protocol(protocol::udp);
	w5500::socket<Port, N>::open();
}

template<class Port, std::size_t N> epl::element::wiznet::w5500::udp::datagram epl::element::wiznet::w5500::udp::socket<Port, N>::recv() noexcept
{
	datagram result;

	result.header = socket::recv_header();
	result.payload = w5500::socket<Port, N>::recv(result.header.size, true);

	return result;
}

template<class Port, std::size_t N> template<std::integral T, std::size_t C> std::size_t epl::element::wiznet::w5500::udp::socket<Port, N>::recv(const std::span<T, C> dest) noexcept
{
	const auto header{socket::recv_header()};

	const auto payload_size{std::min(header.size, dest.size())};
	const auto tail_size{header.size - payload_size};

	const auto result{w5500::socket<Port, N>::recv(dest.first(header.size), true)};

	if (tail_size)
	{
		std::vector<std::uint8_t> temp(tail_size);
		w5500::socket<Port, N>::recv(std::span{temp}, true);
	}

	return result;
}

template<class Port, std::size_t N> epl::element::wiznet::w5500::udp::datagram::header_type epl::element::wiznet::w5500::udp::socket<Port, N>::recv_header() noexcept
{
	datagram::header_type result;

	const auto buf{w5500::socket<Port, N>::recv(s_header_size, true)};

	std::copy(begin(buf), begin(buf) + 4, begin(result.source_address));
	result.source_port = (buf[4] << 8) | buf[5];
	result.size = (buf[6] << 8) | buf[7];

	return result;
}

template<class Port, std::size_t N> std::size_t epl::element::wiznet::w5500::udp::socket<Port, N>::available_to_read() noexcept
{
	const auto total_size{epl::element::wiznet::w5500::socket<Port, N>()};
	return total_size >= s_header_size ? total_size - s_header_size : 0;
}
