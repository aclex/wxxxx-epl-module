/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_STATUS_H
#define EPL_ELEMENT_WIZNET_W5500_STATUS_H

#include <array>

#include <epl/state.h>

namespace epl::element::wiznet::w5500
{
	enum class status : unsigned char
	{
		closed = 0x00,
		init = 0x13,
		listen = 0x14,
		synsent = 0x15,
		synrecv = 0x16,
		established = 0x17,
		fin_wait = 0x18,
		closing = 0x1a,
		time_wait = 0x1b,
		close_wait = 0x1c,
		last_ack = 0x1d,
		udp = 0x22,
		macraw = 0x42
	};
}

#endif // EPL_ELEMENT_WIZNET_W5500_STATUS_H
