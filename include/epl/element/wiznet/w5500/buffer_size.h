/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_BUFFER_SIZE_H
#define EPL_ELEMENT_WIZNET_W5500_BUFFER_SIZE_H

#include <array>

#include <epl/state.h>

namespace epl::element::wiznet::w5500
{
	enum class buffer_size : unsigned char
	{
		s0kb = 0,
		s1kb = 1,
		s2kb = 2,
		s4kb = 4,
		s8kb = 8,
		s16kb = 16
	};

	std::size_t in_bytes(const buffer_size b) noexcept
	{
		switch (b)
		{
		using enum buffer_size;
		default:
		case s0kb:
			return 0;

		case s1kb:
			return 0x0400;

		case s2kb:
			return 0x0800;

		case s4kb:
			return 0x1000;

		case s8kb:
			return 0x2000;

		case s16kb:
			return 0x4000;
		}
	}
}

#endif // EPL_ELEMENT_WIZNET_W5500_BUFFER_SIZE_H
