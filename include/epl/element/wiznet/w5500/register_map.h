/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_ELEMENT_WIZNET_W5500_REGISTER_MAP_H
#define EPL_ELEMENT_WIZNET_W5500_REGISTER_MAP_H

namespace epl::element::wiznet::w5500::register_map
{
	constexpr std::uint8_t common{0};

	template<std::size_t N> class socket
	{
		static_assert(N < 8, "8 sockets (0-7) are supported by W5500.");

		static constexpr std::uint8_t base{static_cast<std::uint8_t>(N << 2)};
	public:
		static constexpr std::uint8_t control{base + 1};
		static constexpr std::uint8_t tx{base + 2};
		static constexpr std::uint8_t rx{base + 3};
	};
}

#endif // EPL_ELEMENT_WIZNET_W5500_REGISTER_MAP_H
