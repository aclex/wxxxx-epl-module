/*
	wiznet-epl-element - Wiznet ethernet controller element library for EPL
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <array>
#include <span>

#include <epl/thread.h>
#include <epl/spi.h>
#include <epl/irq.h>

#include <epl/element/wiznet/w5500/device.h>
#include <epl/element/wiznet/w5500/socket.h>

using namespace std;

using namespace epl;
using namespace epl::spi;

using namespace epl::element::wiznet::w5500;

namespace
{
	using miso = gpio::pin<gpio::port::a, 6>;
	using mosi = gpio::pin<gpio::port::a, 7>;
	using sck = gpio::pin<gpio::port::a, 5>;
	using nss = gpio::pin<gpio::port::a, 4>;
	using irt = gpio::pin<gpio::port::a, 2>;

	using spi0 = spi::port<0, sck, miso, mosi, nss>;
	using my_device = device<spi0>;
	using my_socket = socket<spi0, 0>;

	using led = gpio::pin<gpio::port::b, 0>;
}

int main(int, char**)
{
	epl::irq::unit::reset();
	epl::irq::unit::set_level_bits(3);

	epl::irq::unit::enable();

	led::configure(gpio::output_mode::push_pull); // or gpio::configure<led>(...)

	led::set(epl::state::off);

	spi0::configure(spi::mode::master, duplex::full, spi::word_length::b8, spi::prescaler::p256, spi::bit_order::msb, spi::nss_mode::output);

	irt::configure(gpio::mode::input);

	net::address::ip::v4 bc_addr{255, 255, 255, 255};

	net::address::mac hw_addr{'t', 'e', 's', 't', 'm', 'e'}; // 74:65:73:74:6d:65
	net::address::mac bc_hw_addr{0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

	my_device::reset();

	this_thread::sleep_for(1s);

	my_device::refresh();
	my_device::set_source_hardware_address(span{hw_addr});
	my_device::template bind_interrupt<irt>();
	my_socket::enable_interrupt_handling();
	my_device::upload();

	my_socket::close();
	my_socket::set_protocol(protocol::udp);
	my_socket::set_source_port(1025);
	my_socket::set_destination_port(1026);
	my_socket::set_destination_address(bc_addr);
	my_socket::set_destination_hardware_address(bc_hw_addr);
	my_socket::set_tx_buffer_size(buffer_size::s2kb);
	my_socket::set_rx_buffer_size(buffer_size::s2kb);

	my_socket::open();

	my_socket::send(span{bc_hw_addr});

	led::set(epl::state::on);

	while(true)
	{
		epl::irq::unit::wait_for_interrupt();
	}
}
